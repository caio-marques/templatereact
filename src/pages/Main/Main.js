import React  from 'react';

import containerImage from '../../assets/bg-template.png'

import {
    Container, Content, Title
} from './styles';

export default function Main() {
    return (
        <Container>
            <Content>
                <Title>Este é um tamplete de React Native!!</Title>
            </Content>
        </Container>
    );
}

