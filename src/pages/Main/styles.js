import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding-top: 10px;
  background-color: #001A2E;
  justify-content: center;
`;

export const Content = styled.View`
  align-items: center;
  padding: 0 50px;
`;

export const Title = styled.Text`
  font-size: 25px;
  color: #fff;
  text-align: center;
`;
