import React from 'react';
import Routes from './routes';
import { StatusBar } from 'react-native';


const App = () => (

<>  
    <StatusBar barStyle="light-content" backgroundColor="#001A2E" /> 
    <Routes />
</>
);

export default App;


// StatusBar --> barra de notificações no android 