# Template React

Este é um template pessoal para React Native

### Faça o download do projeto

```
git clone https://caio-marques@bitbucket.org/caio-marques/templatereact.git
```

## Instalando Dependencias

Todas dependencias estão indicadas no *package.json* e pelo o npm é possivel instala-las.

```
npm install
```
De forma a garnatir que o projeto tenha visão de todas dependencias do projeto, linque todas dependencias com o react.

```
react-native link
```

## Instalando Outras Dependencias

As dependencias que deseja instalar a par do projeto incial devem seguir o mesmo processo acima, mas é indicado o nem da dependencia, é automaticamente já indicada no *package.json*.

```
npm i <nome_da_dependencia> 
```

E linque a dependencias, há caso que não ira acontecer nada, pois a dependencia, não necessita desse processo.

```
react-native link <nome_da_dependencia>
```

## Rodando o projeto e Construindo o Projeto 

### Emulador local e device

O projeto pode ser rodado em emlador ou no proprio device, mas não está gerando o *.apk* propriamente dito, este recurso é utilizado para vizualiação do app.

```
react-native run-android
```

Ele irá para a forma de excução possivel.

Há casos que o projeto, pode dar erro relacionados ao proprio desenvolvimento, e o servidor do node guarda um cache da apliação muitas vezes não a atualizando de forma correta. Desta forma recomendo que feche o servidor node, e rode o comando a baixo pra limpar o cache do servidor.

```
react-native start --reset-cache
```

Então rode o app novamente.

### Construindo o projeto

Para gerar o *.apk*, ou construir o projeto é necessario que gere uma chave para versionamento do release e que faça algumas configurações indicadas no link a baixo.

* [Passo-a-passo](https://tableless.com.br/react-native-build-release-android/) - Gerando chave para a construção do release.

Desta forma é possivel dar o build no projeto com os comandos:

```
cd android
&&
./gradlew assembleRelease
```
Há casos que o *assembleRelease* não funciona por motivos variados, mas há outra alternativa para o build:

```
./gradlewgit  build
```

Desta forma seu *.apk* estará pronto para ser instalado em qualquer device, e ele se encontra na pasta:

```
android\app\build\outputs\apk\release
```
